# osysv-core

## Description
The OpenSysV core system. It includes the kernel, basic utilities (all /bin, /sbin), and core libraries (e.g. libc)

## Installation
TODO

## Support
You can make issues regarding bugs you found, future improvements to the codebase, or general questions.

## Contributing
All contributions are accepted, but they are subject to review before final merger.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
All modification to the base SVR4 codebase are published under the 3-clause BSD License.
